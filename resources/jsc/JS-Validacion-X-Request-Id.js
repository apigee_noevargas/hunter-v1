var trazabilidad_request_id;
 try{
     
    trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
    validacionHeaders();
    

 } catch (err) {
    context.setVariable('flow.error.code', '400.hunter.001');
    context.setVariable('flow.error.message', "Verificar los datos ingresados.");
    context.setVariable('flow.error.folio', (trazabilidad_request_id===null || trazabilidad_request_id==="")?context.getVariable("messageid"):trazabilidad_request_id);
    context.setVariable('flow.error.detalle', err);
    context.setVariable('flow.error.http.code', 400);
    context.setVariable('flow.error.http.reason', 'Bad Request');
 }
 
 function validacionHeaders(){
     
    if (context.getVariable("request.header.x-request-id") !== null) {
        if(context.getVariable("request.header.x-request-id").trim() === ""){
            throw mensajeValidacionHeader("x-request-id");
        }
    } else {
        throw mensajeValidacionHeader("x-request-id");
    }
    
 }
 
 function mensajeValidacionHeader(msg){return "Campo header [" + msg + "], no cumple con las restricciones establecidas en el contrato."}