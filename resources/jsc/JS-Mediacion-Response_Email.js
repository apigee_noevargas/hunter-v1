var body_response;
var headerOrigin;
var response_content;
var trazabilidad_request_id;
var json_content;
var recursos;



 try{
     
    headerOrigin = context.getVariable('request.header.origin');
    response_content = context.getVariable('response.content');
    context.setVariable('message.header.Access-Control-Allow-Origin', headerOrigin);
    context.setVariable('message.header.Content-Type', 'application/json');
    trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
    recursos = new Array();
    
    print(response_content);
    
    json_content = JSON.parse(response_content);
    
    json_content.data.sources.forEach((item)=>{
        recursos.push({
            "dominio": item.domain,
            "uri": item.uri,
            "fechaPrimerUso": item.extracted_on,
            "fechaUltimoUso": item.last_seen_on,
            "enUso": item.still_on_page
        });
    });
    
    
    body_response = {
        "mensaje": "Operación exitosa",
        "folio": trazabilidad_request_id,
        "resultado": {
            "estatusEmail": json_content.data.status,
            "estatusVerificacion": json_content.data.result,
            "aviso": json_content.data._deprecation_notice,
            "puntos": json_content.data.score,
            "email": json_content.data.email,
            "recursos": recursos
        }
    }
    
    context.setVariable("response.content", JSON.stringify(body_response));

 }catch(err){
     print(err);
 }