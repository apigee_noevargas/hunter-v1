var headerOrigin;
var response_content;
var statusCode;
var trazabilidad_request_id;

try {
    
    headerOrigin = context.getVariable('request.header.origin');
    response_content = context.getVariable('response.content');
    statusCode = context.getVariable('message.status.code');
    trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
     
    context.setVariable('message.header.Access-Control-Allow-Origin', headerOrigin);
    context.setVariable('message.header.Content-Type', 'application/json; charset=utf-8');

    json_content = JSON.parse(response_content);


    context.setVariable("response.content", JSON.stringify(responseErr()));

  

} catch (err) {
    print("error: " + err.message);
}


function responseErr() {
    
    if(statusCode=="401"){
        statusCode = "500";
        context.setVariable('message.status.code',statusCode);
        context.setVariable('message.reason.phrase', 'Internal Server Error');
    }
    else{
        context.setVariable('message.status.code',statusCode);
    }

    var errorArr = new Array();
    var id_error = json_content.errors[0].id;

    json_content.errors.forEach(function(item) {
            errorArr.push(
                item.details
            );
    });

    return {
        "codigo": statusCode+".hunter.001",
        "mensaje": id_error,
        "folio": trazabilidad_request_id,
        "detalles": errorArr
    }
}