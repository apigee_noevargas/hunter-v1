var trazabilidad_request_id;
var correo;
 
try{
    
    context.setVariable('message.header.Content-Type', 'application/json');
    trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
    
    correo = context.getVariable("request.queryparam.email");
    
    var pattern = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
    
    if(correo!==null){
        if(correo !== ""){
            if (!pattern.test(correo)) {
            throw "El formato del correo es incorrecto";
            }
        }else{
            throw "Campo Query email, no cumple con las restricciones establecidas en el contrato.";
        }
        
    }else{
        throw "Campo Query email, no cumple con las restricciones establecidas en el contrato.";
    }
    
    
}catch(err){
    context.setVariable('flow.error.code', '400.hunter.001');
    context.setVariable('flow.error.message', "Verificar los datos ingresados.");
    context.setVariable('flow.error.folio', (trazabilidad_request_id===null)?context.getVariable("messageid"):trazabilidad_request_id);
    context.setVariable('flow.error.detalle', err);
    context.setVariable('flow.error.http.code', 400);
    context.setVariable('flow.error.http.reason', 'Bad Request');
}