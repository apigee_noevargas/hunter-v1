var proxy_pathsuffix;
var request_verb;
var body_response;
var headerOrigin;
var response_content;
var trazabilidad_request_id;
var json_content;
var atributos;
 
 try{
        headerOrigin = context.getVariable('request.header.origin');
        response_content = context.getVariable('response.content');
        context.setVariable('message.header.Access-Control-Allow-Origin', headerOrigin);
        context.setVariable('message.header.Content-Type', 'application/json');
        proxy_pathsuffix = context.getVariable('proxy.pathsuffix');
        request_verb = context.getVariable('request.verb');
        trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
        atributos = new Array();
        print(response_content);
    
        json_content = JSON.parse(response_content);
        
     if(/^\/atributos\/[0-9a-zA-Z]{0,}$/.test(proxy_pathsuffix)){
         
         if(request_verb === "GET"){
            atributosJsonResponse();
         }
         
     }else if(/^\/atributos$/.test(proxy_pathsuffix)){
        if(request_verb === "GET"){
            atributosList()
        }else if(request_verb === "POST"){
            atributosJsonResponse();
        }
     }
 
     context.setVariable("response.content", JSON.stringify(body_response));
 } catch (err) {

    print("error: " + err.message);

}

function atributosJsonResponse(){
    body_response = {
          "mensaje": "Operación exitosa",
          "folio": trazabilidad_request_id,
          "resultado": {
            "atributo": {
              "idAtributo": json_content.data.id,
              "etiqueta": json_content.data.label
            }
        }
    }
}

function atributosList(){
    json_content.data.leads_custom_attributes.forEach((item)=>{
        atributos.push(
            {
            "idAtributo": item.id,
            "etiqueta": item.label
          }
        );
    });
    
    
    
    body_response = {
          "mensaje": "Operación exitosa",
          "folio": trazabilidad_request_id,
          "resultado": {
            "total": json_content.meta.total,
            "atributos": atributos
        }
    }
}


