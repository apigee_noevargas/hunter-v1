var trazabilidad_request_id;
var id;
var request_content;
var proxy_pathsuffix;
var request_verb;
var jsonRequest;
 
try{
    
    context.setVariable('message.header.Content-Type', 'application/json');
    trazabilidad_request_id = context.getVariable("flow.trazabilidad.request.id");
    proxy_pathsuffix = context.getVariable('proxy.pathsuffix');
    request_verb = context.getVariable('request.verb');
 
      if(/^\/atributos\/[0-9a-zA-Z]{0,}$/.test(proxy_pathsuffix)){
         
         if(request_verb === "GET"){
             atributosPorId();
         }else if(request_verb === "PUT"){
             atributosPorIdAndJsonRequest();
         }else if(request_verb === "DELETE"){
             atributosPorId();
        }
         
     }else if(/^\/atributos$/.test(proxy_pathsuffix)){
        if(request_verb === "GET"){
             atributosPorId();
        }else if(request_verb === "POST"){
             atributosPorJsonRequest();
        }
     }
    
}catch(err){
    print(err);
    context.setVariable('flow.error.code', '400.hunter.001');
    context.setVariable('flow.error.message', "Verificar los datos ingresados.");
    context.setVariable('flow.error.folio', (trazabilidad_request_id===null)?context.getVariable("messageid"):trazabilidad_request_id);
    context.setVariable('flow.error.detalle', err);
    context.setVariable('flow.error.http.code', 400);
    context.setVariable('flow.error.http.reason', 'Bad Request');
}



function validacionId(){
    id = context.getVariable("atributo.id");
    
    var pattern = /^[0-9a-zA-Z]{1,}$/;
    
    if(!pattern.test(id)){
        throw "Campo Path Variables idAtributo, no cumple con las restricciones establecidas en el contrato.";
    }
}

function mediacionJsonRequest(){
    request_content = JSON.parse(context.getVariable('request.content'));
    jsonRequest = {
            "label": request_content.etiqueta
        }
    context.setVariable('request.content', JSON.stringify(jsonRequest));
}



function atributosPorId(){
    validacionId();
}

function atributosPorIdAndJsonRequest(){
    validacionId();
    mediacionJsonRequest();
}

function atributosPorJsonRequest(){
    mediacionJsonRequest();
}